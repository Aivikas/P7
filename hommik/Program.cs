﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hommik
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene i1 = new Inimene
            {
                Isikukood = "47708030241",
                EesNimi = "Aivi",
                PereNimi = "Murd"
            };

            Console.WriteLine(i1);

            Inimene i2 = new Inimene
            {
                Isikukood = "37810180313",
                EesNimi = "Raiko",
                PereNimi = "Suurna"
            };

            Console.WriteLine(i2);

            Inimene i3 = new Inimene
            {
                Isikukood = "50004130226",
                EesNimi = "Markus",
                PereNimi = "Murulauk"
            };

            Console.WriteLine(i3);

            Inimene i4 = new Inimene
            {
                Isikukood = "60810157032",
                EesNimi = "Elis Ester",
                PereNimi = "Suurna"
            };
            Console.WriteLine(i4);
        }
    }
}

