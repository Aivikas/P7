﻿using System;
using System.Collections.Generic;

namespace hommik
{
    class Inimene
    {
        public static List<Inimene> Inimesed = new List<Inimene>();

        private string _EesNimi;
        private string _PereNimi;
        private string _Isikukood;

        public string EesNimi
        {
            get => _EesNimi;
            set
            {
                if (value.Length > 1)
                    _EesNimi = value.Substring(0, 1).ToUpper() + value.Substring(1).ToLower();
                else _EesNimi = value.ToUpper();
            }
        }

         
        public string PereNimi
        {
            get => _PereNimi;
            set
            {
                if (value.Length > 1)
                    _PereNimi = value.Substring(0, 1).ToUpper() + value.Substring(1).ToLower();
                else _PereNimi = value.ToUpper();
            }
        }

        public string Isikukood
        {
        get {
                return _Isikukood;
            }
            set { _Isikukood = value; }
        }


        public override string ToString()
        {
            return $"{EesNimi} {PereNimi} ({_Isikukood})" ;
        }

    }
}
    

